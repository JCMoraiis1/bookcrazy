package Entidades;

import java.util.Scanner;

public class Funcionario extends Pessoa {

    private String setor;
    private int matricula;
    
    public Funcionario() {
		super();
	}

	public Funcionario(String setor, int matricula, String nome, String sobrenome,String cpf, String endereco) {
        super(nome, sobrenome, cpf, endereco);
        this.setor = setor;
        this.matricula = matricula;
    }

	public String getSetor() {
        return setor;
    }

    public void setSetor(String setor) {
        this.setor = setor;
    }

    public int getMatricula() {
        return matricula;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }
    public String toString() {
    	String setor1 = setor;
    	int matricula1 = matricula;
		String nome1 = nome;
		String sobrenome1 = sobrenome;
		String cpf1 = cpf;
		String endereco1 = endereco;
		return "Setor: " + setor1 + " Matricula: " + matricula1 + " Nome do funcion�rio: " + nome1 + " Sobrenome: " + sobrenome1 + " CPF: " + cpf1 + " Endere�o: " + endereco1;
	}  

}
