
package Entidades;
import java.util.Scanner;
import Negocio.NegocioCliente;

public class Cliente extends Pessoa {
	
            
    public Cliente(String nome, String sobrenome, String cpf, String endereco) {
        super(nome, sobrenome, cpf, endereco);
    }
    public String toString() {
		String nome1 = nome;
		String sobrenome1 = sobrenome;
		String cpf1 = cpf;
		String endereco1 = endereco;
		return "Nome do cliente: " + nome1 + " Sobrenome: " + sobrenome1 + " CPF: " + cpf1 + " Endere�o: " + endereco1;
	}  
    public Cliente () {
    	
    }
}
