package Negocio;
/**
 * 
 * @author J�lio Oliveira.
 *
 */
public class NegocioUtils {
	
	//Cria��o dos objetos necess�rios para serem manipulados na classe.
	
	//Construtor da classe.
	public NegocioUtils(){}
	
	/**
	 * M�todo que valida e formata se o CPF cont�m 11 d�gitos, ou se n�o h� d�gito diferente de n�mero.
	 * @param cpf
	 * @return
	 */
	public boolean validaCpf(String cpf) {
		if (cpf.length() == 11 && validaAtributoApenasNumeros(cpf) == true) {
				return true;
		}
		return false;
	}
	
	/**
	 * M�todo que valida se o nome ou sobrenome n�o cont�m n�meros. 
	 * @param nomeSobrenome
	 * @return
	 */
	public boolean validaNomeSobrenome(String nomeSobrenome) {
		for (int i = 0; i < nomeSobrenome.length(); i++) {
			char ch = nomeSobrenome.charAt(i);
			if (Character.isDigit(ch)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * M�todo que valida se o atributo do tipo String cont�m apenas n�meros.
	 * @param atributo
	 * @return
	 */
	public boolean validaAtributoApenasNumeros(String atributo) {
		for (int i = 0; i < atributo.length(); i++) {
			char ch = atributo.charAt(i);
			if (!(ch >= 0 || ch <= 9)) {
				System.out.println("O cpf n�o pode conter o caracter: " + ch);
				return false;
			}
		}
		return true;
	}
	
	/**
	 * M�todo para formatar CPF.
	 * @param cpf
	 * @return
	 */
	public String formatarCpf(String cpf){
		String parte1 = cpf.substring(0,3);
		String parte2 = cpf.substring(3,6);
		String parte3 = cpf.substring(6,9);
		String parte4 = cpf.substring(9,11);
		return parte1 + "." + parte2 + "." + parte3 + "-" + parte4;
	}
	
	
}
