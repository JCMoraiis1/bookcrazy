package Negocio;

import Entidades.Funcionario;
import Entidades.Funcionario;
import Negocio.NegocioUtils;

import java.util.ArrayList;
import java.util.Scanner;
import Dados.FuncionarioDados;

public class NegocioFuncionario extends Funcionario {
	
	//Cria��o dos objetos necess�rios para serem manipulados na classe.
	NegocioUtils negocioUtils = new NegocioUtils();
	FuncionarioDados funcionarioDados = new FuncionarioDados();
	
	//Construtor da classe sem atributos.
	public NegocioFuncionario() {}
	
	//Construtor da classe com todos os atributos.
	public NegocioFuncionario(String setor, int matricula, String nome, String sobrenome,String cpf, String endereco) {
        super(setor, matricula, nome, sobrenome, cpf, endereco);
    }
	
	/**
	 * M�todo para para chamar o cadastrarFuncionario de FuncionarioDados, se caso validar as informa��es.
	 * @param funcionario
	 * @return
	 */
	public boolean cadastrarFuncionario(Funcionario funcionario) {
		String matricula = Integer.toString(funcionario.getMatricula());
		if (negocioUtils.validaCpf(funcionario.getCpf()) == false) {
			return false;
		} else if (negocioUtils.validaNomeSobrenome(funcionario.getNome()) == true || negocioUtils.validaNomeSobrenome(funcionario.getSobrenome()) == true) {
			return false;
		} else if (negocioUtils.validaAtributoApenasNumeros(matricula) == false) {
			return false;
		}
		funcionario.setCpf(negocioUtils.formatarCpf(funcionario.getCpf()));
		return funcionarioDados.cadastrarFuncionario(funcionario);
	}
	
	/**
	 * M�todo que retorna o ArrayList de Funcionarios para a classe FuncionarioApresenta��o.
	 * @return
	 */
	public ArrayList<Funcionario> listarFuncionario() {
		return funcionarioDados.listarFuncionario();
	
	}
	/**
	 * M�todo que chama o m�todo remover da classe FuncionarioDados.
	 * @param posicao
	 * @return
	 */
	public boolean removerFuncionario(int posicao){
		return funcionarioDados.removerFuncionario(posicao);
	}
	/**
	 * M�todo que atualiza o Funcionario, se suas informa��es forem validadas.
	 * @param posicao
	 * @param Funcionario
	 * @return
	 */
	public boolean atualizarFuncionario(int posicao,NegocioFuncionario funcionario){
		
		if (negocioUtils.validaCpf(funcionario.getCpf()) == false) {
			return false;
		} else if (negocioUtils.validaNomeSobrenome(getNome()) == true || negocioUtils.validaNomeSobrenome(getSobrenome()) == true) {
			return false;
		}
		return funcionarioDados.atualizarFuncionario(posicao,funcionario);
	}
}