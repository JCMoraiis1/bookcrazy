/**
 *
 * @author matheus,waschislin,julio cesar
 */
package Negocio;

import Entidades.Livro;

import java.util.ArrayList;

import Dados.LivroDados;

public class NegocioLivro extends Livro {
	
	//Cria��o dos objetos necess�rios para serem manipulados na classe.
	LivroDados livroDados = new LivroDados();

	/**
	 * Construtor da classe com todos os par�metros.
	 * @param nomeLivro
	 * @param autor
	 * @param isbn
	 * @param editora
	 * @param numPag
	 */
	public NegocioLivro(String nomeLivro, String autor, Long isbn, String editora,int numPag){
		super(nomeLivro,autor,isbn,editora,numPag);
	}
	
	//Construtor da classe sem par�metros.
	public NegocioLivro() {}
	
	/**
	 * M�todo para verificar se as informa��es de ISBN do livro � v�lida, sendo assim o livro ser� cadastrado.
	 * @param livro
	 * @return
	 */
	public boolean cadastrarLivro(NegocioLivro livro) {
		if (livro.validarIsbn(livro.getIsbn()) == true) {
			return livroDados.cadastrarLivro(livro);
		}
		return false;
	}
	
	/**
	 * M�todo para validar se o ISBN cont�m 10 caracteres(antiga conven��o) ou 13 caracteres.
	 * @param isbn
	 * @return
	 */
	private boolean validarIsbn(Long isbn) {
		String StringIsbn = Long.toString(isbn);
		if (StringIsbn.length() == 10 || StringIsbn.length() == 13) {
			return true;
		}
		return false;
	}
	
	//M�todo que retorna um ArrayList de Livro para ser mmostrado na classe ApresentacaoLivro.
	public ArrayList<Livro> listarLivros() {
		return livroDados.listarLivros();
	}
	
	/**
	 * M�todo que captura a posi��o do livro a ser removido ap�s isso chama o m�todo removerLivro da classe LivroDados.
	 * @param posicao
	 * @return
	 */
	public boolean removerLivro(int posicao){
		return livroDados.removerLivro(posicao);
	}
	
	/**
	 * M�todo que passa a posi��o e o objeto livro a ser alterado, para o m�todo atualizarLivro da classe livroDados.
	 * @param posicao
	 * @param livro
	 * @return
	 */
	public boolean alterarLivro(int posicao, Livro livro){
		return livroDados.atualizarLivro(posicao, livro);
	}
}
