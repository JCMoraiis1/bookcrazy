package Dados;
import java.util.ArrayList;

import Entidades.Funcionario;

public class FuncionarioDados extends Funcionario {
	
		public static ArrayList<Funcionario> dadosFuncionario = new ArrayList<Funcionario>();
		
		/**
		 * M�todo que adiciona um Funcionario ao ArrayList.
		 * @param Funcionario
		 * @return
		 */
		public boolean cadastrarFuncionario(Funcionario funcionario) {
			return dadosFuncionario.add(funcionario);
		}
		
		/**
		 * M�todo que retorna todo o ArrayList para ser listado.
		 * @return
		 */
		public ArrayList<Funcionario> listarFuncionario() {
			return dadosFuncionario;
		}
		
		/**
		 * M�todo que remove o Funcionario de acordo com uma posi��o passada como par�metro para o m�todo e ArrayList.
		 * @param posicao
		 * @return
		 */
		public boolean removerFuncionario(int posicao) {
			dadosFuncionario.remove(posicao);
			return true;
		}
		
		/**
		 * M�todo que atualiza um Funcionario de acordo com a posi��o que recebe como par�metro.
		 * @param posicao
		 * @return
		 */
		public boolean atualizarFuncionario(int posicao,Funcionario funcionario) {
			dadosFuncionario.set(posicao, funcionario);
			return true;
		}
	}
