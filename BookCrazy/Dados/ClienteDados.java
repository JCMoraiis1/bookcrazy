package Dados;

import java.util.ArrayList;
import Entidades.Cliente;
import Entidades.Livro;

/**
 * 
 * @author J�lio Oliveira.
 *
 */
public class ClienteDados {
	public static ArrayList<Cliente> dadosCliente = new ArrayList<Cliente>();
	
	/**
	 * M�todo que adiciona um cliente ao ArrayList.
	 * @param cliente
	 * @return
	 */
	public boolean cadastrarCliente(Cliente cliente) {
		return dadosCliente.add(cliente);
	}
	
	/**
	 * M�todo que retorna todo o ArrayList para ser listado.
	 * @return
	 */
	public ArrayList<Cliente> listarCliente() {
		return dadosCliente;
	}
	
	/**
	 * M�todo que remove o cliente de acordo com uma posi��o passada como par�metro para o m�todo e ArrayList.
	 * @param posicao
	 * @return
	 */
	public boolean removerCliente(int posicao) {
		dadosCliente.remove(posicao);
		return true;
	}
	
	/**
	 * M�todo que atualiza um cliente de acordo com a posi��o que recebe como par�metro.
	 * @param posicao
	 * @return
	 */
	public boolean atualizarCliente(int posicao,Cliente cliente) {
		dadosCliente.set(posicao, cliente);
		return true;
	}
}
